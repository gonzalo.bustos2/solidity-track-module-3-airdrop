import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const deployFunc: DeployFunction = async (_hre: HardhatRuntimeEnvironment) => {
  const { ethers, deployments, getNamedAccounts } = _hre;
  const { deployer } = await getNamedAccounts();
  const { deploy } = deployments;
  console.log("Deploying Airdrop contract...");
  const deployedAirdrop = await deploy("Airdrop", {
    from: deployer,
    proxy: {
      proxyContract: "OpenZeppelinTransparentProxy"
    }
  });

  console.log("Deployed Airdrop: ", deployedAirdrop.address);
  // we assume Token has been deployed because of the hardhat-deploy dependencies
  const tokenDeploy = await deployments.get("Token");
  const airdropFactory = await ethers.getContractFactory("Airdrop");
  const airdrop = await airdropFactory.attach(deployedAirdrop.address);
  const setTokenTx = await airdrop.setToken(tokenDeploy.address);
  await setTokenTx.wait();
  console.log("setToken Transaction hash: ", setTokenTx.hash);
  const tokenFactory = await ethers.getContractFactory("Token");
  const token = await tokenFactory.attach(tokenDeploy.address);
  const setAirdropTx = await token.setAirdrop(deployedAirdrop.address);
  await setAirdropTx.wait();
  console.log("setAidrop Transaction hash: ", setAirdropTx.hash);
};
export default deployFunc;

deployFunc.id = "deployed_Airdrop"; // id required to prevent reexecution
deployFunc.tags = ["Airdrop"];
deployFunc.dependencies = ["Token"]; // Run deployToken first and then deploy Airdrop