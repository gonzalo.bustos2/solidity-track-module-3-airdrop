import { deployments } from "hardhat";
import { expect } from "chai";
import { ethers, getNamedAccounts } from "hardhat";
import { Token, Token__factory } from "../typechain";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";


describe("Token", () => {
  let deployer: string;
  let sampleAirdrop: SignerWithAddress;
  let token: Token;

  before(async () => {
    ({ deployer } = await getNamedAccounts());
    [, sampleAirdrop] = await ethers.getSigners();
  });

  const setupTest = deployments.createFixture(async ({ deployments }) => {
    await deployments.fixture();
    const tokenContract = await deployments.get("Token");
    token = Token__factory.connect(tokenContract.address, ethers.provider.getSigner());
  });

  beforeEach(async () => {
    await setupTest();
  });

  it("Proper Token deployment and initialization", async () => {
    expect(await token.name()).to.equal("Token");
    expect(await token.symbol()).to.equal("TKN");
    expect(await token.hasRole(await token.DEFAULT_ADMIN_ROLE(), deployer)).to.be.true;
  });

  it("Should revert mint if not airdrop", async () => {
    const airdropAddress = await sampleAirdrop.getAddress();
    await expect(token.connect(sampleAirdrop).mint(airdropAddress, 10)).to.be.reverted;
  });

  it("Should mint if airdrop", async () => {
    const airdropAddress = await sampleAirdrop.getAddress();
    await token.setAirdrop(airdropAddress);
    await expect(token.connect(sampleAirdrop).mint(airdropAddress, 10)).not.to.be.reverted;
    expect(await token.balanceOf(airdropAddress)).to.equal(10);
  });
});
