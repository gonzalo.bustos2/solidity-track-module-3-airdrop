// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

contract Token is Initializable, ERC20Upgradeable, AccessControlUpgradeable {
    bytes32 public constant AIRDROP_ROLE = keccak256("AIRDROP_ROLE");

    /// @custom:oz-upgrades-unsafe-allow constructor
    // solhint-disable-next-line
    constructor() initializer {}

    function initialize(address defaultAdmin) initializer public {
        __ERC20_init("Token", "TKN");
        __AccessControl_init();

        _grantRole(DEFAULT_ADMIN_ROLE, defaultAdmin);
    }

    function setAirdrop(address airdrop_) external onlyRole(DEFAULT_ADMIN_ROLE) {
        grantRole(AIRDROP_ROLE, airdrop_);
    }

    function mint(address to_, uint256 amount_) external onlyRole(AIRDROP_ROLE) {
        _mint(to_, amount_);
    }

    uint256[50] private __gap;
}

