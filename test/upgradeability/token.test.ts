import { expect } from "chai";
import { deployments, ethers, getNamedAccounts } from "hardhat";
import { ContractTransaction } from "ethers";
import { Airdrop, Airdrop__factory, Token, Token__factory } from "../../typechain";
import { getProofForLeaf, root } from "../tree";


describe("Upgrades", function () {
    let deployer: string;
    let airdrop: Airdrop;
    let token: Token;
    let realClaimer: string;

    before(async () => {
        ({ deployer } = await getNamedAccounts());
        realClaimer = "0x4470c60F988bfabdaA045405a53a220B644600B8";
    });
    const setupTest = deployments.createFixture(async ({ deployments }) => {
        await deployments.fixture();
        const tokenContract = await deployments.get("Token");
        token = Token__factory.connect(tokenContract.address, ethers.provider.getSigner());
        const airdropContract = await deployments.get("Airdrop");
        airdrop = Airdrop__factory.connect(airdropContract.address, ethers.provider.getSigner());
        await airdrop.setRoot(root);
    });
      
  describe("GIVEN an airdroppable contract was deployed and set up and upgraded to remove the possibility to mint", function () {
    before(async function () {
        await setupTest();
        const deployAdmin = await deployments.getOrNull("DefaultProxyAdmin");
        if (deployAdmin === null) throw new Error("DefaultProxyAdmin not deployed. Make sure you deployed the right proxy.");
        const tokenV2Contract = await ethers.getContractFactory("TokenV2");
        const tokenV2 = await tokenV2Contract.connect(await ethers.getSigner(deployer)).deploy();
        const deployedAdmin = new ethers.Contract(deployAdmin.address, deployAdmin.abi).connect(
            await ethers.getSigner(deployer),
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            ) as any;
        console.log("deployAdmin", deployedAdmin.upgrade);
        await deployedAdmin.upgrade(token.address, tokenV2.address);
    });
    it("Upgraded token version should allow deployer to mint", async () => {
        await expect(token.mint(realClaimer, 10)).not.to.be.reverted;
        expect(await token.balanceOf(realClaimer)).to.equal(10);
    });
    it("Upgraded token version should not allow airdrop to mint", async () => {
        const proof = await getProofForLeaf(0);
        await expect(airdrop.claim(proof, 10, realClaimer)).to.be.reverted;
    });
  });
});
