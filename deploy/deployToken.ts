import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const deployFunc: DeployFunction = async (_hre: HardhatRuntimeEnvironment) => {
  const {deployments, getNamedAccounts} = _hre;
  const { deployer } = await getNamedAccounts();
  const {deploy} = deployments;

  console.log("Deploying Token contract...");
  const deployedToken = await deploy("Token", {
    from: deployer,
    proxy: {
      execute: {
        init: {
          methodName: "initialize",
          args: [deployer]
        }
      },
      proxyContract: "OpenZeppelinTransparentProxy"
    }
  });

  console.log("Deployed token: ", deployedToken.address);
};
export default deployFunc;

deployFunc.id = "deployed_Token"; // id required to prevent reexecution
deployFunc.tags = ["Token"];
