import { deployments } from "hardhat";
import { expect } from "chai";
import { ethers, getNamedAccounts } from "hardhat";
import { Airdrop, Airdrop__factory, Token, Token__factory } from "../typechain";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { getProofForLeaf, root } from "./tree";


describe("Aidrop", () => {
  let deployer: string;
  let airdrop: Airdrop;
  let token: Token;
  let account1: SignerWithAddress;
  let realClaimer: string;

  before(async () => {
    ({ deployer } = await getNamedAccounts());
    [, account1] = await ethers.getSigners();
    realClaimer = "0x4470c60F988bfabdaA045405a53a220B644600B8";
  });

  const setupTest = deployments.createFixture(async ({ deployments }) => {
    await deployments.fixture();
    const tokenContract = await deployments.get("Token");
    token = Token__factory.connect(tokenContract.address, ethers.provider.getSigner());
    const airdropContract = await deployments.get("Airdrop");
    airdrop = Airdrop__factory.connect(airdropContract.address, ethers.provider.getSigner());
  });

  beforeEach(async () => {
    await setupTest();
  });

  it("Should not set Token", async () => {
    // The token has already been set on deployment script
    await expect(airdrop.setToken(token.address)).to.be.revertedWith("Token has already been set");
  });

  it("Should set first root", async () => {
    await expect(airdrop.setRoot(root)).not.to.be.reverted;
  });

  it("Should not set second root", async () => {
    await airdrop.setRoot(root);
    await expect(airdrop.setRoot(root)).to.be.revertedWith("Root has already been set");
  });

  it("Should not claim if wrong claimer", async () => {
    await airdrop.setRoot(root);
    const claimer = await account1.getAddress();
    const proof = await getProofForLeaf(0);
    await expect(airdrop.claim(proof, 10, claimer)).to.be.revertedWith("Provided address cannot claim");
    expect(await token.balanceOf(claimer)).to.equal(0);
  });

  it("Should claim if realClaimer", async () => {
    await airdrop.setRoot(root);
    const proof = await getProofForLeaf(0);
    await expect(airdrop.claim(proof, 10, realClaimer)).not.to.be.reverted;
    expect(await token.balanceOf(realClaimer)).to.equal(10);
  });

  it("Should not claim if already claimed", async () => {
    await airdrop.setRoot(root);
    const proof = await getProofForLeaf(0);
    await airdrop.claim(proof, 10, realClaimer);
    await expect(airdrop.claim(proof, 10, realClaimer)).to.be.revertedWith("Already claimed tokens");
    expect(await token.balanceOf(realClaimer)).to.equal(10);
  });
});
