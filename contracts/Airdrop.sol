// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/utils/cryptography/MerkleProofUpgradeable.sol";
import "./Token.sol";

contract Airdrop {
    bytes32 public root;
    Token private token;

    // marking the used leaves means that each (address, amount) pair can only be used once
    mapping(bytes32 => bool) private usedLeaves;

    // We can't set the token/root via constructor because we want the airdrop contract to be upgradeable as well
    function setToken(Token token_) external {
        require(address(token) == address(0), "Token has already been set");
        token = token_;
    }

    function setRoot(bytes32 root_) external {
        require(root == bytes32(0), "Root has already been set");
        root = root_;
    }

    function claim(
        bytes32[] memory proof,
        uint256 amount,
        address claimer
    ) external {
        bytes32 leaf = getLeaf(claimer, amount);
        require(MerkleProofUpgradeable.verify(proof, root, leaf), "Provided address cannot claim");
        require(!usedLeaves[leaf], "Already claimed tokens");
        usedLeaves[leaf] = true;
        token.mint(claimer, amount);
    }

    function getLeaf(address acc, uint256 amount) private pure returns (bytes32) {
        return keccak256(abi.encodePacked(acc, amount));
    }
}