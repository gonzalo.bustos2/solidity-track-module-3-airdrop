import { MerkleTree } from "merkletreejs"
import keccak256 from "keccak256"
import { solidityKeccak256 } from "ethers/lib/utils";

export const leaves = [
    {
        claimer: "0x4470c60F988bfabdaA045405a53a220B644600B8",
        amount: 10
    },
    {
        claimer: "0x543FAB684a1588d10e6d8EA87A293FF15DEEE1E5",
        amount: 15
    },
    {
        claimer: "0x1aa0abcFb8765bCcD292Ac48a7823367402E5E0B",
        amount: 20
    },
    {
        claimer: "0xA39C80330ccFeaa250C3B5493FFb839a64F97851",
        amount: 25
    },
].map(entry => solidityKeccak256(["address","uint256"], [entry.claimer, entry.amount]));

export const badLeaves = [
    {
        claimer: "0x4470c60F988bfabdaA045405a53a220B644600B9",
        amount: 10
    },
    {
        claimer: "0x543FAB684a1588d10e6d8EA87A293FF15DEEE1E5",
        amount: 16
    },
    {
        claimer: "0x1aa0abcFb8765bCcD292Ac48a7823367402E5e0B",
        amount: 20
    },
    {
        claimer: "0xA39C80330ccFeaa250C3B5493FFb839a64F97852",
        amount: 25
    },
].map(entry => solidityKeccak256(["address","uint256"], [entry.claimer, entry.amount]));

export const merkleTree = new MerkleTree(leaves, keccak256, { sortPairs: true });
export const root = merkleTree.getRoot();
export const getProofForLeaf = (index: any) => {
    return merkleTree.getHexProof(leaves[index]);
}