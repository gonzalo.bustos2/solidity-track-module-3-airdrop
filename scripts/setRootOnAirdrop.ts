import { root } from "../test/tree"
import { task } from "hardhat/config";

task("set-root-on-airdrop")
  .setAction(async (_, hre) => {
    await hre.run("deploy");
    const deploy = await hre.deployments.getOrNull("Airdrop");
    if (deploy === null) throw new Error("Airdrop not deployed");
    const airdrop = (await hre.ethers.getContractFactory("Airdrop")).attach(deploy.address);
    const setRootTx = await airdrop.setRoot(root);
    await setRootTx.wait();
    console.log("setRoot Transaction hash:", setRootTx.hash);
  });
